package main
import "os"
import "fmt"

func main() {
  args := os.Args[1]
  _,err := os.Stat(args)
  if err != nil {
    fmt.Println("Error: File does not exist!")
  } else {
    fmt.Println("Error: File exists!")
  }
}

