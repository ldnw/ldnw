# ldnw

LDNW (Language Does Not Work)

# Syntax

## Hello World
```
main {
    pn("Hello, world!")
}
```
# Compiling

Compile the language using the `compiler.go` file in src/ or using the `ldnw` binary in src/bin/ like this:

`ldnw [file]`

`go run compiler.go [file]`

